import java.util.Random;
public class Board{
	
	final int SIZE = 5;
	private Tile[][] grid;
	
	public Board(){
		this.grid = new Tile[SIZE][SIZE];
		Random rng = new Random();
		
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = Tile.BLANK;
				
            }
			int wallPosition = rng.nextInt(grid[i].length);
			grid[i][wallPosition] = Tile.HIDDEN_WALL;
        }
	}
	//override 
	public String toString(){
		String newStr = "";
		for(Tile[] row: grid){
			for(Tile tile: row){
				newStr += tile.getName() + " ";
			}
			newStr += "\n";
		}
		return newStr;
	}
	public int placeToken(int row, int col){
        // Check if row & column input r within bounds
		if(row < 0 || row >= SIZE || col < 0 || col >= SIZE){
			return -2;
		}
        // Check pos is occupied
		else if(grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL){
			return -1;
		}
		// Check if hidden wall in position
		else if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		} else { // Place castle 
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}
