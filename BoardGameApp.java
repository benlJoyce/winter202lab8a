import java.util.Scanner;

public class BoardGameApp {
    public static void main(String[] args) {
	    System.out.println("Welcome to BoardGameApp!");

		int numCastles = 5;
		int turns = 0;
		Board board = new Board();
		Scanner scanner = new Scanner(System.in);
		
		
		while(numCastles > 0 && turns < 8){
			System.out.println(board);
			System.out.println("Number of castles is: " + numCastles);
			System.out.println("Number of turns out of 5 is: " + turns);
			
			System.out.println();
			System.out.println("which row would you like to place your castle tile?");
			int row = scanner.nextInt();
			System.out.println("which column would you like to place your castle tile?");
			int col = scanner.nextInt();
		
			int response = board.placeToken(row,col);
			
			while(response == -2 || response == -1){
				System.out.println("Invalid input :( Please try again! Which row would you like to place your castle tile?");
				row = scanner.nextInt();
				System.out.println("Invalid input :( Please try again! Which column would you like to place your castle tile?");
				col = scanner.nextInt();	
				
				response = board.placeToken(row,col);
			}
			
			if(response == 1){
				System.out.println("Oh no! There was a wall there. Your turn is reduced by 1.");
				turns++;
				System.out.println("You are at: " + turns + " out of 8 turns");
				
			}
			else{
				System.out.println("Castle successfully placed!");
				turns++;
				numCastles--;
				System.out.println("You are at: " + turns + " out of 8 turns. " + "Number of castles: " + numCastles);
			
			}
		}
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("Congrats you win!");
		}
		else{
			System.out.println("You lost.");
		}
	}
}